#!/usr/bin/python3

# Query MIA record for one or more people
# Copyright (C) 2001, 2002, 2003  Martin Michlmayr <tbm@cyrius.com>
# Copyright (C) 2006  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import re, string, sys
import apt_pkg, status

def help(target):
    target.write("""Usage: mia-query [OPTION]... [PATTERN]
Query MIA database for specific records

Default search is on loginname, email address or gpg ID

Options:
  -n, --name                Search anywhere in name/uid (implies -r)
  -p, --package             Search based on package
  -r, --regex               Treat search query as regex (case-insensitive)

""")

def parse_args():
    global patterns
    global Cnf, Options

    apt_pkg.init()
    Cnf = apt_pkg.Configuration()
    apt_pkg.read_config_file_isc(Cnf, status.config)

    Arguments = [("h", "help", "Query::Options::Help"),
                 ("n", "name", "Query::Options::SearchName"),
                 ("p", "package", "Query::Options::SearchPackage"),
                 ("r", "regex", "Query::Options::UseRegex")
                 ]
    for i in ["Help", "SearchName", "SearchPackage", "ShowLog", "UseRegex"]:
        if not Cnf.exists("Query::Options::%s" % i):
            Cnf["Query::Options::%s" % i] = ""
    patterns = apt_pkg.parse_commandline(Cnf, Arguments, sys.argv)
    if Cnf["Query::Options::SearchName"]:
        Cnf["Query::Options::UseRegex"] = "Yes"

    Options = Cnf.subtree("Query::Options")

def main():
    global Options

    parse_args()
    if Options["Help"]:
        help(sys.stdout)
        sys.exit(0)

    if len(patterns) != 1:
        help(sys.stderr)
        sys.stderr.write("Exactly one search term required!\n")
        sys.exit(1)
    pat = patterns[0]
    if not Options["UseRegex"]:
        if Options["SearchPackage"]:
            pat = "maint:"+pat
        elif pat.find('@') >= 0:
            pat = "email:"+pat
        else:
            pat = "ldap:"+pat
        res = status.searchCarnivore(pat)
        if len(res) == 0:
            sys.stderr.write("No entries found\n")
            sys.exit(1)
        for entry in res:
            status.get(entry).prettyPrint()
    else:
        regex = re.compile(pat, re.I)
        package = Options["SearchPackage"]
        for maintainer, data in status.carnivore.iteritems():
            entry = status.get(maintainer)
            found = False
            if package:
                for p in entry.ce.package:
                    if regex.search(p):
                        found = True
            else:
                # FIXME: can go much faster by *just* iterating over keys in
                # carnivore, and not instantiate classes unless needed
                for id in entry.ce.ldap + entry.ce.email:
                    if regex.search(id):
                        found = True
                if Options["SearchName"]:
                    for name in entry.ce.realname:
                        if regex.search(name):
                            found = True
            if found:
                entry.prettyPrint()

main()


# vim: ts=4:expandtab:shiftwidth=4:
