#!/usr/bin/perl

# Copyright (C) 2014-2017 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use warnings;
use strict;
use CGI qw(:standard);
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use DBD::Pg;
use File::Temp qw(tempfile);
use IPC::Open2;
use POSIX qw(strftime);

$CGI::POST_MAX = 0;
$CGI::DISABLE_UPLOADS = 1;

my $cgi = CGI->new;
my $dbh = DBI->connect("dbi:Pg:service=qa", 'guest', '',
    { AutoCommit => 1, RaiseError => 1, PrintError => 1});

my $width = 800;
$width = $cgi->param('width') if ($cgi->param('width') and $cgi->param('width') =~ /^\d+$/);
my $height = 600;
$height = $cgi->param('height') if ($cgi->param('height') and $cgi->param('height') =~ /^\d+$/);
my $from_date = '';
$from_date = "'$1'" if ($cgi->param('from_date') and $cgi->param('from_date') =~ /^(\d+-\d+-\d+)$/);
my $to_date = '';
$to_date = "'$1'" if ($cgi->param('to_date') and $cgi->param('to_date') =~ /^(\d+-\d+-\d+)$/);
my $hlght_date = '';
$hlght_date = "'$1'" if ($cgi->param('hlght_date') and $cgi->param('hlght_date') =~ /^(\d+-\d+-\d+)$/);
my $date_fmt = '%Y-%m';
$date_fmt = $cgi->param('date_fmt') if ($cgi->param('date_fmt') and $cgi->param('date_fmt') =~ /^%[dmyYjHMsSbB](-%[dmyYjHMsSbB])*$/);

sub make_percent($)
{
    my $num = shift;
    if ($cgi->param('want_percent')) {
        return "(100 * $num / \$6)";
    } else {
        return "$num";
    }
}

sub plot_line
{
    my ($data, $lines, $using, $title, $linestyle, $want_ticks) = @_;
    my $titled = "title \"$title\"";
    my $e = int($lines / 15);
    $e = 1 if ($e < 1);
    my $every = "every $e";
    my $titled_line    = "$data $using $titled with lines linetype $linestyle, ";
    my $untitled_line  = "$data $using notitle with lines linetype $linestyle, ";
    my $points_on_line = "$data $every $using $titled"
                    . " with points linetype $linestyle, ";
    if ($want_ticks) {
        return $untitled_line . $points_on_line;
    } else {
        return $titled_line;
    }
}

my @packages = split(/[ ,]+/, $cgi->param('packages') // '');
unless (@packages) {
    print header (-status => '500 Bad Parameters', -type => 'text/plain', -charset => 'utf-8');
    print "At least one package is required\n";
    exit;
}

my $multi = @packages != 1;

# build gnuplot command
my $cmd = "set terminal png size $width,$height\n" .
          "set xdata time\n" .
          "set timefmt \"%Y-%m-%d\"\n" .
          "set format x \"" . $date_fmt . "\"\n" .
          "set pointsize 1.5\n" .
          "set xlabel \"date\"\n" .
          "set xrange [$from_date:$to_date]\n";

if ($hlght_date) {
    $cmd .= "set arrow from $hlght_date, graph 0 to $hlght_date, graph 1 nohead\n";
}

if ($cgi->param('want_percent')) {
    $cmd .= "set ylabel \"percent of submitters\"\n";
} else {
    $cmd .= "set ylabel \"number of submitters\"\n";
}

if ($cgi->param('want_legend')) {
    $cmd .= "set key below\n";
} else {
    $cmd .= "unset key\n";
}

if ($multi) {
    $cmd .= "set title \"popcon graph\"\n";
} else {
    $cmd .= "set title \"@packages popcon graph\"\n";
}

$cmd .= "plot ";

my $linestyle = 0;
my $want_ticks = $cgi->param('want_ticks');
foreach my $package (@packages) {
    my $packagekey = $multi ? "$package " : "";

    my ($fh, $file) = tempfile("popcon.XXXXXX", TMPDIR => 1, UNLINK => 1);
    my $rows = $dbh->selectall_arrayref("select p.day, p.vote, p.old, p.recent, p.no_files, submissions from popcon_package pp join popcon p on (pp.id = p.package_id) join popcon_day pd on (pd.day = p.day) where package = ? and in_debian order by p.day", undef, $package);
    print $fh join (" ", @$_), "\n" foreach (@$rows);
    my $lines = @$rows;
    printf $fh "%s 0 0 0 0 0\n", strftime("%F", gmtime) unless ($lines > 0);
    close $fh;

    my $cmd_seen = 0;
    if ($cgi->param('show_installed')) {
        $linestyle++; $cmd_seen++;
        $cmd .= plot_line(
            " \"$file\"", $lines,
            " using 1:" . make_percent('($2+$3+$4+$5)'),
            "${packagekey}installed", $linestyle, $want_ticks);
    }
    if ($cgi->param('show_vote')) {
        $linestyle++; $cmd_seen++;
        $cmd .= plot_line(
            " \"$file\"", $lines,
            " using 1:" . make_percent('($2)'),
            "${packagekey}vote", $linestyle, $want_ticks);
    }
    if ($cgi->param('show_old')) {
        $linestyle++; $cmd_seen++;
        $cmd .= plot_line(
            " \"$file\"", $lines,
            " using 1:" . make_percent('($3)'),
            "${packagekey}old", $linestyle, $want_ticks);
    }
    if ($cgi->param('show_recent')) {
        $linestyle++; $cmd_seen++;
        $cmd .= plot_line(
            " \"$file\"", $lines,
            " using 1:" . make_percent('($4)'),
            "${packagekey}recent", $linestyle, $want_ticks);
    }
    if ($cgi->param('show_nofiles')) {
        $linestyle++; $cmd_seen++;
        $cmd .= plot_line(
            " \"$file\"", $lines,
            " using 1:" . make_percent('($5)'),
            "${packagekey}no-files", $linestyle, $want_ticks);
    }
    unless ($cmd_seen) {
        print header (-status => '500 Bad Parameters', -type => 'text/plain', -charset => 'utf-8');
        print "At least one of the show_* parameters is required\n";
        exit;
    }
}

$cmd =~ s/, $//; # remove last comma from command built
$cmd .= "\n";

if ($cgi->param('show_cmd')) {
    print header (-type => 'text/plain', -charset => 'utf-8');
    print $cmd;
    exit;
}

print header(-type => 'image/png');

# spawn gnuplot and read output back
my $pid = open2(\*CHLD_OUT, \*CHLD_IN, 'gnuplot', '-');
print CHLD_IN $cmd;
close CHLD_IN;

while (<CHLD_OUT>) {
    print $_;
}
close CHLD_OUT;

waitpid $pid, 0
