#!/usr/bin/perl -w

# Copyright (C) 2006, 2007, 2017 Christoph Berg <myon@debian.org>
# Copyright (C) 2010 Raphael Geissert <geissert@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use strict;
use CGI qw/:cgi/;
use CGI::Carp;
use DBI;
use List::Util qw(max);
use Dpkg::Version qw(version_compare);
use YAML::Syck qw();

$CGI::POST_MAX = 0;
$CGI::DISABLE_UPLOADS = 1;

my $query = new CGI;
my $distribution = 'debian';
my $table_req = 'debian';
my $tprefix = '';
my $tsuffix = '';
my ($sources, $binaries);
my @valid_tables = qw(all new debian ports archived derivatives ubuntu);
my $valid_tables_re = join('|', @valid_tables);

my $translate_suite = {};

{
    my $fh;

    open($fh, '<', '/srv/qa.debian.org/data/RELEASES')
	or die "Could not open RELEASES file: $!";

    while(my $line = <$fh>) {
	chomp($line);
	my ($alias, $release) = split(/\t/, $line);
	if ($alias && $release) {
	    $translate_suite->{$alias} = $release;
	}
    }
    close($fh);
}

my $dbh = DBI->connect("dbi:Pg:service=udd",
			"guest") or die $!;

if ($query->param('table') && $query->param('table') =~ m/^($valid_tables_re)$/o) {
    $table_req = $1;

    $distribution = $table_req unless ($table_req eq 'all');
    if ($table_req eq 'debian') {
	$tprefix = '';
    } else {
	$tprefix = $table_req . '_';
    }
    if ($table_req eq 'new') {
	$tsuffix = '_madison';
    }
}

$sources = "SELECT source,
		    version,
		    release,
		    component AS area,
		    distribution
		FROM ${tprefix}sources${tsuffix}
		WHERE
		    source IN (%s)"
;
$binaries = "SELECT package,
		    version,
		    release,
		    architecture,
		    component AS area,
		    distribution
		FROM ${tprefix}packages${tsuffix}
		WHERE
		    %s IN (%s)"
;

if ($table_req eq 'debian') {
    $sources .= <<EOS;
	AND extra_source_only IS NOT TRUE
EOS
}

my $nomarkup = ( $query->param('yaml') or $query->param('text') ) ? 1 : 0;
if ($query->param('text') or $query->param('yaml')) {
    print "Content-Type: text/plain\n\n";
} else {
    print "Content-Type: text/html\n\n";

    print <<'EOT';
<html>
<head>
<title>dak ls aka madison</title>
<link rel="shortcut icon" href="/favicon.ico">
<link type="text/css" rel="stylesheet" href="/revamp.css">
EOT

    print <<'EOT';
<script type="text/javascript" language="JavaScript">
function objectOver(x){
  x.style.color="blue";
}
function objectOut(x){
  x.style.color="black";
}
function toggle(x) {
  var oContent=document.getElementById(x) ;
  if (oContent.style.display=="block") {
    oContent.style.display="none";
  } else {
    oContent.style.display="block";
  }
}
</script>
EOT

    print <<'EOT';
</head>
<body>
<h1 id="title"><img src="/debian.png" alt="Debian logo" width="258"
height="82" style="vertical-align: middle"> Quality Assurance</h1>

<div id="body">
EOT
}

# Take a string with comma or space-separated items and return it into
# a normalised list with every item surrounded by quotation marks.
# The optional second argument is used for mapping things to other things, e.g.
# suite names to code names.
sub split_n_prep {
    my ($list, $translate) = @_;
    my @list = split(/[\s,]+/, $list); # unpack
    @list = map { exists $translate->{$_} ? $translate->{$_} : $_ } @list # translate
	if $translate;
    $list = join(', ', map { "'$_'" } @list); # quote and pack
    return $list;
}

# Procedure to sort by release
sub sort_rel {
    my $i = 1;
    # we need local copies to modify them:
    my ($_a, $_b) = ($a, $b);

    # Strip archive area, if present
    $_a =~ s,/(?:contrib|non-free|non-free-firwmare)$,,;
    $_b =~ s,/(?:contrib|non-free|non-free-firwmare)$,,;

    # How to sort releases by name
    my %releases = ('etch'    => $i++,
		    'lenny'   => $i++,
		    'squeeze' => $i++,
		    'wheezy'  => $i++,
		    'jessie'  => $i++,
		    'stretch' => $i++,
		    'buster'  => $i++,
		    'bullseye' => $i++,
		    'bookworm' => $i++,
		    'trixie'  => $i++,
		    'sid'     => $i++,
		    );

    my ($na, $nb);

    # Assign and compare by release order
    $na = (exists($releases{$_a})? $releases{$_a} : 0);
    $nb = (exists($releases{$_b})? $releases{$_b} : 0);
    return ($na <=> $nb) unless (($na + $nb) == 0);

    # If comparing two releases that don't match our table, compare
    # them by name
    return ($_a cmp $_b);
}

# sort by architecture name, sources first
sub sort_arch {
    return -1 if ($a eq 'source');
    return 1 if ($b eq 'source');
    return ($a cmp $b);
}

# Given the release name, the distribution and the archive area, it
# returns a pretty and short name to refer to it
sub construct_rel_name {
    my ($release, $dist, $area) = @_;

    # abbreviate release and dist names to make them easier to read
    $release =~ s/proposed-updates/p-u/;
    $dist    =~ s/debian-(backports)/$1/;

    $release = (($dist ne $distribution)? "$dist/" : '') . $release;
    $release .= ($area ne 'main')? "/$area" : '';

    return $release;
}

my $token_re = "[a-z0-9][a-z0-9.+-]+";
my $token = "^($token_re)\$";
my $token_list = "^((?:$token_re\[ ,]*)+)\$";
my $opt = 0;

my $all_pkgs = 0;
my $arch_req = '';
if ($query->param('a') and $query->param('a') =~ /$token_list/io) {
    $opt = 1;
    $arch_req = $1;
    my $arch = split_n_prep($arch_req);
    my $extra = <<EOS;
	AND architecture IN ($arch)
EOS
    $sources  .= $extra if ($arch !~ m/'source'/);
    $binaries .= $extra;
}
my $area_req = '';
if ($query->param('c') and $query->param('c') =~ /$token_list/io) {
    $opt = 1;
    $area_req = $1;
    my $area = split_n_prep($area_req);
    my $extra = <<EOS;
	AND component IN ($area)
EOS
    $sources  .= $extra;
    $binaries .= $extra;
}
my $release_req = '';
if ($query->param('s') and $query->param('s') =~ /$token_list/io) {
    $opt = 1;
    $release_req = $1;
    my $release = split_n_prep($release_req, $translate_suite);
    my $extra = <<EOS;
	AND release IN ($release)
EOS
    $sources  .= $extra;
    $binaries .= $extra;
}
if (defined($query->param('S'))) {
    $opt = 1;
    $all_pkgs = 1;
}

my $package_req = '';
if ($query->param('package') and $query->param('package') =~ /$token_list/io) {
    $package_req = $1;
    my $packages = split_n_prep($package_req);

    if (!$nomarkup) {
	print "<h2>dak ls</h2>\n";
        print("<pre>\n");
    }
    $| = 1;

    my $ssth = $dbh->prepare(sprintf($sources, $packages));
    my $bsth = $dbh->prepare(sprintf($binaries,
				    $all_pkgs? 'source' : 'package',
				    $packages));
    $ssth->execute() or die $!;
    $bsth->execute() or die $!;

    my %maxl = (
		'package' => 0,
		'version' => 0,
		'release' => 0,
		);

    my %data;

    while (my $d = $ssth->fetchrow_hashref("NAME_lc")) {
	$d->{'release'} = construct_rel_name($d->{'release'}, $d->{'distribution'}, $d->{'area'});
	$d->{'architecture'} = 'source';

	$maxl{'package'} = max($maxl{'package'}, length($d->{'source'}));
	$maxl{'version'} = max($maxl{'version'}, length($d->{'version'}));
	$maxl{'release'} = max($maxl{'release'}, length($d->{'release'}));

	$data{$d->{'source'}} = {}
	    unless defined ($data{$d->{'source'}});
	$data{$d->{'source'}}{$d->{'version'}} = {}
	    unless defined ($data{$d->{'source'}}{$d->{'version'}});
	$data{$d->{'source'}}{$d->{'version'}}{$d->{'release'}} = []
	    unless defined ($data{$d->{'source'}}{$d->{'version'}}{$d->{'release'}});

	push @{$data{$d->{'source'}}{$d->{'version'}}{$d->{'release'}}}, $d->{'architecture'};
    }

    while (my $d = $bsth->fetchrow_hashref("NAME_lc")) {
	$d->{'release'} = construct_rel_name($d->{'release'}, $d->{'distribution'}, $d->{'area'});

	$maxl{'package'} = max($maxl{'package'}, length($d->{'package'}));
	$maxl{'version'} = max($maxl{'version'}, length($d->{'version'}));
	$maxl{'release'} = max($maxl{'release'}, length($d->{'release'}));

	$data{$d->{'package'}} = {}
	    unless defined ($data{$d->{'package'}});
	$data{$d->{'package'}}{$d->{'version'}} = {}
	    unless defined ($data{$d->{'package'}}{$d->{'version'}});
	$data{$d->{'package'}}{$d->{'version'}}{$d->{'release'}} = []
	    unless defined ($data{$d->{'package'}}{$d->{'version'}}{$d->{'release'}});

	push @{$data{$d->{'package'}}{$d->{'version'}}{$d->{'release'}}}, $d->{'architecture'};
    }

    if (defined($query->param('yaml'))) {
	print YAML::Syck::Dump(\%data);
	exit;
    }
    my $format = " %-$maxl{package}s | %-$maxl{version}s | %-$maxl{release}s | %s\n";

    # Output is sorted by:
    # package,
    foreach my $package (sort keys %data) {
	my $gversion;
	# then by version,
	foreach my $version (sort version_compare keys %{$data{$package}}) {
	    $gversion = $version;
	    # and finally by release
	    foreach my $release (sort sort_rel keys %{$data{$package}->{$version}}) {
		my $ref = $data{$package}->{$version}{$release};
		printf $format,
				$package,
				$version,
				$release,
				join(', ', sort sort_arch @$ref);
	    }
	}
	if (defined($query->param('g')) || defined($query->param('G'))) {
	    $opt = 1;
	    print "\n";

	    printf "%s (>= %s)\n", $package, $gversion
		if defined($query->param('g'));
	    printf "%s (>> %s)\n", $package, $gversion
		if defined($query->param('G'));
	}
    }

    if (!$nomarkup) {
        print "</pre>\n";
    }
}

exit if ($nomarkup);

print "<p>
<form action=\"#\" method=\"get\">
<input type=\"text\" name=\"package\" value=\"" . $package_req . "\">
in <select name=\"table\">
";
for my $table (@valid_tables) {
    printf ('<option value="%1$s" %2$s>%1$s</option>\n', $table, ($table eq $table_req)? 'selected' : '');
}
print "</select>
<input type=\"submit\" value=\"Query\">
<br>
<small> Show:
<span onmouseover='javascript:objectOver(this)' onmouseout='javascript:objectOut(this)' onclick='javascript:toggle(\"config\")'>More options</span>,
<span onmouseover='javascript:objectOver(this)' onmouseout='javascript:objectOut(this)' onclick='javascript:toggle(\"help\")'>help</span>
</small><br>
<div id=\"config\" style=\"display:". ($opt ? "block" : "none") ."\">
<table border=\"0\">
<tr><td>architecture</td><td><input type=\"text\" name=\"a\" value=\"" . ($arch_req || "") . "\"></td></tr>
<tr><td>component</td><td><input type=\"text\" name=\"c\" value=\"" . ($area_req || "") . "\"></td></tr>
<tr><td><label for=g>greaterorequal</label></td><td><input type=\"checkbox\" name=\"g\" id=g" . ($query->param('g') ? " checked" : "") ."></td></tr>
<tr><td><label for=G>greaterthan</label></td><td><input type=\"checkbox\" name=\"G\" id=G" . ($query->param('G') ? " checked" : "") . "></td></tr>
<tr><td>suite</td><td><input type=\"text\" name=\"s\" value=\"" . ($release_req || "") . "\"></td></tr>
<tr><td><label for=S>source-and-binary</label></td><td><input type=\"checkbox\" name=\"S\" id=S" . ($query->param('S') ? " checked" : "") ."></td></tr>
<tr><td><label for=text>text-only</label></td><td><input type=\"checkbox\" name=\"text\" id=text></td></tr>
</table>
</div>
</form>

<p>
<div id=\"help\" style=\"display:none\">
<pre>
Usage: dak ls [OPTION] PACKAGE[...]
Display information about PACKAGE(s).

  -a, --architecture=ARCH    only show info for ARCH(s)
  -b, --binary-type=TYPE     only show info for binary TYPE
  -c, --component=COMPONENT  only show info for COMPONENT(s)
  -g, --greaterorequal       show buildd 'dep-wait pkg &gt;= {highest version}' info
  -G, --greaterthan          show buildd 'dep-wait pkg &gt;&gt; {highest version}' info
  -h, --help                 show this help and exit
  -r, --regex                treat PACKAGE as a regex <i>[not supported in madison.cgi]</i>
  -s, --suite=SUITE          only show info for this suite
  -S, --source-and-binary    show info for the binary children of source pkgs

ARCH, COMPONENT and SUITE can be comma (or space) separated lists, e.g.
    --architecture=amd64,i386
</pre>
</div>
";

print <<'EOT';
<div class="footer"><p>
Made by Raphael Geissert based on work by Christoph Berg.<br>
Copyright (C) 1999-2009 <a
href="https://www.spi-inc.org/" title="SPI, Inc.">Software in the Public Interest</a> and others;
See <a href="https://www.debian.org/license">license terms</a>.
</p></div>
</body>
</html>
EOT
