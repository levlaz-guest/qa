#use wml::templ::template title="RC bugs count - explanation" author="Andreas Barth"

<h1>RC bugs count - explanation</h1>

<p>We only count RC-bugs in Sid that are neither tagged
squeeze-ignore nor fixed, and only of packages that are also in Squeeze.
Also, only actual packages are show, not pseudo-packages.</p>

<p>Rationale: If these bugs are dealt with, and the updated packages have gone
in squeeze, than squeeze is releaseable from a strictly technical point of view.
However, from a not so technical point of view, squeeze needs more for release
than just to be bug free itself, e.g. finishing of the debian-installer. And
for updated packages in Squeeze, there is the excuses list.</p>

<p>This page is quite new, so bugs here are to be feared.</p>

<p>Explanation for bug tags:</p>
<ul>
<li><font color="#f040d0"><strong>P</strong>: pending</font></li>
<li><font color="#00aa00"><strong>+</strong>: patch</font></li>
<li><font color="#ffaa30"><strong>H</strong>: help</font></li>
<li><font color=""><strong>M</strong>: moreinfo</font></li>
<li><font color=""><strong>R</strong>: unreproducible</font></li>
<li><font color="red"><strong>S</strong>: security</font></li>
<li><font color=""><strong>U</strong>: upstream</font></li>
<li><font color="blue"><strong>C</strong>: claimed</font></li>
<li><font color="blue"><strong>D</strong>: upload to delayed or also assigned to ftp.d.o / qa.d.o</font></li>
<li><font color="blue"><strong>&lt;</strong>: removal hinted by one of the release managers</font></li>
<li><font color="gray"><strong>N</strong>: newer than two weeks</font></li>
<li><font color="gray"><strong>T</strong>: only present in testing, not in sid</font></li>
<li><font color="gray"><strong>B</strong>: only present in sid, not in testing (please note that if the package is removed from testing,
the bug is not shown at all anymore)</font></li>
<li><font color=""><strong>=</strong>: merged (means: there exists a bug with a smaller number with which the bug is merged)</font></li>
</ul>

<p>The current package list of the day packages are printed in red (this will be extended some day to allow selecting a package list). Currently [2004-08-22] this list is the list of frozen packages.</p>


<a name=ignore>
<h2>Explanation for ignore reasons:</h2>
<ul>
<li>Ignore my distribution: Hide bugs that are tagged sid or squeeze, respectively. Note, packages that are not in squeeze at all are never shown, this is only to hide sid-only bugs (for example). If you check 'both', you will only get to see bugs that are (supposed to be) present in <b>both</b> sid <i>and</i> squeeze.
<li>Ignore by 'hinted / delayed upload': Ignore bugs that are either hinted (a
Release Manager has decided this package is to be removed from squeeze, and it
will get removed soon) or for which there is an upload in the 'delayed' upload queue (probably a NMU) already waiting to get in the archive.
<li>Ignore by 'claimed / assigned to ftp/qa': Hide bugs that are either
    already claimed to be working on by somebody else (<a
    href="https://bugs.debian.org/usertag:bugsquash@qa.debian.org">claims
    page</a>), or that someone asked for it to be removed ('assigned to
    ftp/qa')
<li>new: Hide bugs less than N days old (see the last textbox to define what is N)
<li>pending: hide bugs tagged pending, meaning: the maintainer claims to have
fixed the bug, but not yet uploaded the package
<li>patched: Hide bugs where a patch is available in the BTS
<li>merged: hide all second and later reports of the same bug
<li>not in the highlighted package list: Hide bugs on non-frozen packages
<li>contrib/non-free: hide bugs on packages that are in the contrib of non-free sections respectively
<li>pseudo-bug to keep sid version out: Hide bugs that are purely there to prevent the new version in sid to progress to squeeze, without being a bug on itself
</ul>

<h4>History</h4>

<ul>
<li>2004-10-03 copes better with multipackages bugs, but delayed and multi-packages doesn't always work
<li>2004-07-07 package list of the day instead of standard or above is red
<li>2004-06-07 added this page
</ul>
