<?php
# keep the header in php generated file
<protect>/*
+-------------------------------------------------------------------------+
| Copyright (C) 2002 Igor Genibel http://genibel.org/                     |
| Copyright (C) 2005--2014 Christoph Berg <myon@debian.org>               |
|                                                                         |
| This program is free software; you can redistribute it and/or           |
| modify it under the terms of the GNU General Public License             |
| as published by the Free Software Foundation; either version 2          |
| of the License, or (at your option) any later version.                  |
|                                                                         |
| This program is distributed in the hope that it will be useful,         |
| but WITHOUT ANY WARRANTY; without even the implied warranty of          |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           |
| GNU General Public License for more details.                            |
+-------------------------------------------------------------------------+
| $Id: developer.wml 1493 2007-01-31 17:12:09Z myon $
+-------------------------------------------------------------------------+

*/</protect>

//apd_set_pprof_trace();

// global variables
$prefix="/srv/qa.debian.org/data/ddpo/results";
$filter_re = '/^[^<>&"]+$/'; // filter against special HTML characters
$counter_span = 0;
$popcon_url = "popcon-graph.php?packages=";
$popcon_instonly = "&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1";

function start_output ()
{
    header('Content-Type: text/html; charset="utf-8"');
    header('Content-Script-Type: text/javascript');
    header('Content-Style-Type: text/css');
    header('Pragma: no-cache');

?>
#use wml::templ::template title="Popularity Contest Statistics" author="Igor Genibel, Christoph Berg, and others. Graphs by Ian Lynagh"

{#style#:
  <link rel="stylesheet" type="text/css" href="developer.css">
:##}

#include "collexp.wmi"

<p>
<?php
}

include("common-html.php");

function print_popcon_entry($binary, $popcon_db, $back_tr_color)
{
    global $popcon_url;
    global $popcon_instonly;

    $row = html_tr(
        html_td(html_b($binary)) .
        html_td(dba_fetch("pi:$binary", $popcon_db)) .
        html_td(sprintf("%.2f%%", 100 * dba_fetch("pi:$binary", $popcon_db) / dba_fetch("pimax:", $popcon_db))) .
        html_td(dba_fetch("pir:$binary", $popcon_db)) .
        html_td(dba_fetch("pv:$binary", $popcon_db)) .
        html_td(sprintf("%.2f%%", 100 * dba_fetch("pv:$binary", $popcon_db) / dba_fetch("pvmax:", $popcon_db))) .
        html_td(dba_fetch("pvr:$binary", $popcon_db)) .
        html_td(dba_fetch("po:$binary", $popcon_db)) .
        html_td(sprintf("%.2f%%", 100 * dba_fetch("po:$binary", $popcon_db) / dba_fetch("pomax:", $popcon_db))) .
        html_td(dba_fetch("por:$binary", $popcon_db)) .
        html_td(dba_fetch("pr:$binary", $popcon_db)) .
        html_td(sprintf("%.2f%%", 100 * dba_fetch("pr:$binary", $popcon_db) / dba_fetch("prmax:", $popcon_db))) .
        html_td(dba_fetch("prr:$binary", $popcon_db)) .
        html_td(dba_fetch("pn:$binary", $popcon_db)) .
        html_td(sprintf("%.2f%%", 100 * dba_fetch("pn:$binary", $popcon_db) / dba_fetch("pnmax:", $popcon_db))) .
        html_td(dba_fetch("pnr:$binary", $popcon_db)) .
        html_td(html_a("Graph", $popcon_url . rawurlencode($binary)) . " (" .
                html_a("inst only", $popcon_url . rawurlencode($binary) . $popcon_instonly) . ")"
        ),
        "", $back_tr_color
    );
    return $row;
}

function print_popcon($package)
{
    global $prefix;
    global $pack_db;
    global $counter_span;
    global $popcon_url;
    global $popcon_instonly;

    $binaries = dba_fetch("bin:$package", $pack_db);
    if (!$binaries) {
        header('Status: 404 Not Found', FALSE, 404);
        start_output();
        print html_h("No Popularity contest entry for $package", 2);
        return;
    }

    start_output();
    print html_h("Popularity contest statistics for $package", 2);
    $help = html_ul(
        html_li("<b>Name</b> is the package name") .
        html_li("<b>Inst.</b> is the number of people who installed this package (sum of the four categories below)") .
        html_li("<b>Vote</b> is the number of people who use this package regularly") .
        html_li("<b>Old</b> is the number of people who installed, but do not use this package regularly") .
        html_li("<b>Recent</b> is the number of people who upgraded this package recently") .
        html_li("<b>No Files</b> is the number of people whose entry did not contain enough information (atime and ctime were 0)") .
        html_li("Within parenthesis the number represents the number of registered popcon users that sent information about this topic"),
        "small");
    print html_collexp($counter_span, html_b("Help"), "general", true, $help);

    $table = array();
    $back_tr_color = '';
    $popcon_db = dba_open("$prefix/../../popcon/popcon.db", 'r', 'db4');
    $binary_arr = explode(" ", $binaries);
    foreach ($binary_arr as $bin_package) {
        $table[] = print_popcon_entry($bin_package, $popcon_db, $back_tr_color);
        $back_tr_color = $back_tr_color ? "" : "#dcdcdc";
    }

    if (count($binary_arr) > 1) {
        $table[] = html_tr(
            html_td(html_em("Source")) .
            html_td(html_blank(), "", 3) .
            html_td(html_blank(), "", 3) .
            html_td(html_blank(), "", 3) .
            html_td(html_blank(), "", 3) .
            html_td(html_blank(), "", 3) .
            html_td(html_a("Graph", $popcon_url . rawurlencode($binaries)) . " (" .
                    html_a("inst only", $popcon_url . rawurlencode($binaries) . $popcon_instonly) . ")"
            ),
            "", $back_tr_color
        );
    }

    print html_table(
        html_tr(
            html_th("Name", "", 2, 1) .
            html_th("Inst (". dba_fetch("pimax:", $popcon_db) .")", "1st: ". dba_fetch("pimax:n", $popcon_db), 1, 3) .
            html_th("Vote (". dba_fetch("pvmax:", $popcon_db) .")", "1st: ". dba_fetch("pvmax:n", $popcon_db), 1, 3) .
            html_th("Old (". dba_fetch("pomax:", $popcon_db) .")", "1st: ". dba_fetch("pomax:n", $popcon_db), 1, 3) .
            html_th("Recent (". dba_fetch("prmax:", $popcon_db) .")", "1st: ". dba_fetch("prmax:n", $popcon_db), 1, 3) .
            html_th("No Files (". dba_fetch("pnmax:", $popcon_db) .")", "1st: ". dba_fetch("pnmax:n", $popcon_db), 1, 3) .
            html_th("Graph", "", 2, 1)) .
        html_tr(
            html_th("Number") . html_th("%") . html_th("Rank") .
            html_th("Number") . html_th("%") . html_th("Rank") .
            html_th("Number") . html_th("%") . html_th("Rank") .
            html_th("Number") . html_th("%") . html_th("Rank") .
            html_th("Number") . html_th("%") . html_th("Rank")),
        $table, "", "small");

    $img = "<img alt=\"popcon graph for $package\" src=\"/cgi-bin/popcon-png?packages=" . rawurlencode($binaries) . $popcon_instonly . "\">";
    print html_p(html_a($img, $popcon_url . rawurlencode($binaries) . $popcon_instonly));
    #print html_p("<a href=\"/cgi-bin/popcon-png?want_percent=on&amp;packages=" . rawurlencode($binaries) . $popcon_instonly . "\">percent submitters popcon graph for $package</a>");

    print html_a("Raw data for the graph", "/cgi-bin/popcon-data?packages=" . rawurlencode($binaries), "", "JSON");

    print html_p(html_a("Maintainer page", "developer.php?package=$package", ""));
}

$pack_db = dba_open("$prefix/archive.db", 'r', 'db4');

$package = isset($_GET['package'])? $_GET['package'] : '';

if ($package && preg_match($filter_re, $package)) {
    print_popcon($package);

} else {
    start_output();
    $form_data = html_input_text("package");
    print html_form("popcon.php", "GET", "Popcon statistics for ", $form_data, "Go");

}

?>

# vim:sw=4:ft=php:et:
