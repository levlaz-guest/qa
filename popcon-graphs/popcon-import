#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use Time::HiRes qw(time);

my $logfile = "/srv/qa.debian.org/log/popcon.log";
my $verbose = (-t 1);
my $start_time = time;

sub logger ($)
{
    my $message = shift;
    chomp $message;
    if ($verbose) {
        print STDERR "$message\n";
    }
    open LOG, ">> $logfile";
    printf LOG "%s popcon-import[$$] %s\n", scalar(localtime), $message;
    close LOG;
}

$SIG{__WARN__} = sub {
	logger ("Warning: @_");
	return 1;
};

$SIG{__DIE__} = sub {
	logger ("Error: @_");
	return 1;
};

my $dbh = DBI->connect("dbi:Pg:service=qa", 'qa', '', {AutoCommit => 0, RaiseError => 1});

# get package ids
my $pkglist = $dbh->prepare ("SELECT id, package FROM popcon_package");
$pkglist->execute;
my %pkg;
while (my ($id, $package) = $pkglist->fetchrow_array) {
    $pkg{$package} = $id;
}

my $insert_package_id = $dbh->prepare ("INSERT INTO popcon_package (package) VALUES (?) RETURNING id");
sub package_id ($)
{
    my $package = shift;
    return $pkg{$package} if (exists $pkg{$package});
    $insert_package_id->execute ($package);
    my ($id) = $insert_package_id->fetchrow_array;
    $pkg{$package} = $id;
    return $id;
}

my $update_package = $dbh->prepare ("UPDATE popcon_package SET day = ?, vote = ?, old = ?, recent = ?, no_files = ? WHERE id = ?");

my ($done_to) = $dbh->selectrow_array ("SELECT max(day) FROM popcon_day");

my $raw_root = "/srv/scratch/qa.debian.org/all-popcon-results";
chdir "$raw_root" or die "Failed to change to directory $raw_root: $!\n";

for my $file (sort <popcon-*.gz>) {
    next if ($file =~ /^popcon-2004-01-2[23].gz$/); # the first files had a different format
    $file =~ /^popcon-(\d\d\d\d-\d\d-\d\d).gz$/ or die "$file: invalid filename";
    my $day = $1;
    next if ($day le $done_to);
    print "Doing $file ... " if ($verbose);

    my @release;
    my @architecture;
    my @vendor;
    my @package;
    open FILE, "gzip -cd $file |" or die "Failed to open $file: $!\n";
    while (<FILE>) {
        if (/^Submissions: +(\d+)$/) {
            $dbh->do("INSERT INTO popcon_day (day, submissions) VALUES (?, ?)", undef, $day, $1);

        } elsif (/^Release: (\S+)\s+(\d+)$/) {
            push @release, join ("\t", $1, $day, $2);

        } elsif (/^Architecture: (\S+)\s+(\d+)$/) {
            push @architecture, join ("\t", $1, $day, $2);

        } elsif (/^Vendor: (\S+)\s+(\d+)$/) {
            push @vendor, join ("\t", $1, $day, $2);

        } elsif (/^Package: ([a-z0-9][-a-z0-9.+]+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)$/) {
            my ($id, $vote, $old, $recent, $no_files) = (package_id($1), $2, $3, $4, $5);
            push @package, join ("\t", $id, $day, $vote, $old, $recent, $no_files);
	    # note most recent data per package for fast querying
	    $update_package->execute($day, $vote, $old, $recent, $no_files, $id);

        } elsif (/^Package: (\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)$/) {
            # valid package line, but with a weird package name (upper case etc.), ignore it

        } else {
            die "$file:$.: Unexpected line $_";
        }
    }
    close FILE;

    $dbh->do ("COPY popcon_release FROM STDIN");
    foreach my $row (@release) {
        $dbh->pg_putcopydata("$row\n");
    }
    $dbh->pg_putcopyend();

    $dbh->do ("COPY popcon_architecture FROM STDIN");
    foreach my $row (@architecture) {
        $dbh->pg_putcopydata("$row\n");
    }
    $dbh->pg_putcopyend();

    $dbh->do ("COPY popcon_vendor FROM STDIN");
    foreach my $row (@vendor) {
        $dbh->pg_putcopydata("$row\n");
    }
    $dbh->pg_putcopyend();

    $dbh->do ("COPY popcon FROM STDIN");
    foreach my $row (@package) {
        $dbh->pg_putcopydata("$row\n");
    }
    $dbh->pg_putcopyend();

    $dbh->commit;

    logger sprintf("Imported %s in %.1fs", $file, time - $start_time);
    $start_time = time;
}

# remove older input files
system "find /srv/scratch/qa.debian.org/all-popcon-results -type f -mtime +30 -delete";
