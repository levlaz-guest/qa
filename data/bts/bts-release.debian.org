#!/usr/bin/perl

# Copyright (C) 2023 Christoph Berg <myon@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# found_versions ARRAY(0x55bcf355f850)
# bug_num 1029651
# found
# keywords pending bullseye
# fixed_date ARRAY(0x55bcf355b7e0)
# date 1674678961
# summary
# 1032577
# id 1032577
# package release.debian.org
# found_date ARRAY(0x55bcf3733108)
# fixed
# fixed_versions ARRAY(0x55bcf372f168)
# done Paul Gevers <elbrus@debian.org>
# mergedwith
# location db-h
# source
# last_modified 1678363743
# severity normal
# blockedby
# tags
# originator Drew Parsons <dparsons@debian.org>
# archived 0
# forwarded
# owner
# blocks
# affects src:opendrop
# subject unblock: opendrop/3.3.1-5
# pending done
# log_modified 1678363743
# msgid <167835569524.531090.9130973871014922749.reportbug@sandy>
# outlook
# summary
# date 1678362481
# fixed_date ARRAY(0x55bcf372e9a0)
# keywords
# found
# bug_num 1032577
# found_versions ARRAY(0x55bcf37335d0)
# unarchived

use strict;
use DB_File;
use Devscripts::Debbugs;

my $bugs = Devscripts::Debbugs::select ("package:release.debian.org");
my $status = Devscripts::Debbugs::status($bugs);

my %package;
foreach my $bug (keys %$status) {
	#print("$bug\n");
	my $subject = $status->{$bug}->{subject};
	my $done = $status->{$bug}->{done};

	# extract package name from subject
	my $pkg = $subject; # bullseye-pu: package segemehl/0.3.4-3+deb11u1 (Pre-approval)
	next unless $pkg =~ s/^\S+: +//; # remove everything up to first :, otherwise skip
	$pkg =~ s/^package //; # remove "package" noise word
	$pkg =~ s/(?<!,) .*//; # remove everything from first space (unless part of a list)

	foreach my $p (split(/ /, $pkg)) {
		$p =~ s/[_\/,].*//; # strip /version part
		#print("$p $bug $subject\n");
		$package{$p} .= "<li>";
		$package{$p} .= "<del>" if ($done);
		$package{$p} .= "<a href=\"https://bugs.debian.org/$bug\">#$bug: $subject</a>";
		$package{$p} .= "</del>" if ($done);
		$package{$p} .= "</li>\n";
	}
}

my %release_db;
my $release_db_file = tie %release_db, "DB_File", "release.debian.org.new.db",
	O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE or die "Can't open database release.debian.org.new.db : $!";

foreach my $pkg (keys %package) {
	$release_db{$pkg} = $package{$pkg};
}

rename "release.debian.org.new.db", "release.debian.org.db";
