# Copyright 2004 Frank Lichtenheld

package BinSrcMap;

use strict;
use warnings;

use Exporter;

our @ISA = qw( Exporter );
our @EXPORT = qw( get_bin get_src );

# we get the information from the Packages files since
# we can't trust the Binary field in the Sources files.
our $PACKAGES_FILES = '/srv/qa.debian.org/data/ftp/sarge/all/Packages.*.all';

my %binsrc_map;
my %srcbin_map;

sub init {
    open PKGS, "-|", "cat $PACKAGES_FILES | egrep '^(Package:|Source)'"
	or die "Couldn't get package info: $!";

    my $pkg;
    while (<PKGS>) {
	next unless m/^(?:Package:|Source:)/;
	
	if (my ($newpkg) = m/^Package:\s*(.+)$/) {
	    if ($pkg) {
		$binsrc_map{$pkg} = $pkg;
		$srcbin_map{$pkg} = [] unless defined $srcbin_map{$pkg};

		push @{$srcbin_map{$pkg}}, $pkg;
#		print STDERR "(BinSrcMap) $pkg\t=>\t$pkg\n";
	    }
	    $pkg = $newpkg;
	}
		
	if (my ($srcpkg) = m/^Source:\s*(.+)$/) {
	    if ($pkg) {
		$binsrc_map{$pkg} = $srcpkg;
		$srcbin_map{$srcpkg} = [] unless defined $srcbin_map{$srcpkg};

		push @{$srcbin_map{$srcpkg}}, $pkg;
#		print STDERR "(BinSrcMap) $pkg\t=>\t$srcpkg\n";
	    } else {
		die "Stray Source field encountered: $_";
	    }
	    $pkg = undef;
	}
    }

    close PKGS or warn;
}

sub get_src {
    return $binsrc_map{$_[0]};
}

sub get_bin {
    return @{$srcbin_map{$_[0]}};
}

1;
