#!/usr/bin/perl

# Copyright 2004 Frank Lichtenheld

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/lib";

use Getopt::Long;
use Pod::Usage;

use BinSrcMap;
use BugStats;
use PopconStats;
use LintianStats;

$BinSrcMap::PACKAGES_FILES = "/home/djpig/debian/qa/merkel_rsync/data/ftp/sarge/all/Packages*";
$BugStats::BUG_STATS_FILE = '/home/djpig/debian/qa/merkel_rsync/data/ddpo/results/bugs.txt';
$PopconStats::POPCON_STATS_FILE = "/home/djpig/debian/qa/merkel_rsync/data/popcon-stats.txt";
$LintianStats::LINTIAN_STATS_FILE = "/home/djpig/debian/qa/merkel_rsync/data/lintian-list.txt";

my ( $debug, $verbose, $quiet, $version, $help );
my ( @stats, $head );

my %opthash = (
	       "head|n:i" => \$head,
	       "stats|s" => \@stats,
	       "debug|d" => \$debug,
	       "verbose|v" => \$verbose,
	       "quiet|q" => \$quiet,
	       "version" => \$version,
	       "help|h" => \$help,
	       );

Getopt::Long::config('no_getopt_compat', 'no_auto_abbrev');

GetOptions(%opthash) or 
    pod2usage(-exitval => 2, -verbose => 0, 
	      -message => "error parsing options");

pod2usage(-exitval => 0, -verbose => 1) if $help;
if ($version) {
    print <<EOV;
output_stats
Copyright (C) 2004 Frank Lichtenheld
EOV
    exit;
}

$debug ||= 0;
$verbose ||= 0; 
$quiet ||= 0;
$verbose ||= $debug;
$quiet &&= ! $verbose;

$head = 10 if defined($head) and not $head;

unless (@stats) {
    @stats = qw( lintian popcon bugs );
}
my %stats;
foreach (@stats) {
    $stats{$_}++;
}

if ($stats{popcon}) {
    &PopconStats::init();
}
if ($stats{bugs}) {
    &BugStats::init();
}
if ($stats{lintian}) {
    &LintianStats::init();
}

my %pkg_stats;
while (<>) {

    my ($pkg) = m/^\s*([\w+.-]+)/o;

    if ($stats{popcon}) {
	my $pcs = get_popcon_status($pkg);
	$pkg_stats{$pkg}{sum} ||= 0;
	$pkg_stats{$pkg}{sum} -= $pcs;
	push @{$pkg_stats{$pkg}{stats}}, sprintf( "PC: % 5.1f", -$pcs );
    }
    if ($stats{bugs}) {
	my $bs = get_bug_status($pkg) || 0;
	$pkg_stats{$pkg}{sum} ||= 0;
	$pkg_stats{$pkg}{sum} += $bs;
	push @{$pkg_stats{$pkg}{stats}}, sprintf( "BUG: % 5.1f", $bs );
    }
    if ($stats{lintian}) {
	my $ls = get_lintian_status($pkg) || 0;
	$pkg_stats{$pkg}{sum} ||= 0;
	$ls = 100 if $ls > 100;
	$pkg_stats{$pkg}{sum} += $ls;
	push @{$pkg_stats{$pkg}{stats}}, sprintf( "LINT: % 5.1f", $ls );
    }
}

sub by_sum {
    return $pkg_stats{$b}{sum} <=> $pkg_stats{$a}{sum};
}

my $rank = 1;
foreach my $pkg ( sort by_sum keys %pkg_stats ) {
    printf "%4d %-29s => % 6d  %s\n", $rank++, $pkg, $pkg_stats{$pkg}{sum},
    "@{$pkg_stats{$pkg}{stats}}";

    last if $head && $rank > $head;
} 


__END__

=head1 NAME

output_stats

=head1 SYNOPSIS

B<create_package_pages> [I<options>]

B<create_package_pages> B<--help>

B<create_package_pages> B<--version>

For allowed options see the L<"OPTIONS"> section.

=head1 OPTIONS

=over 4

=item B<--debug>, B<-d>

If set, the script will be very, very verbose.
Implies B<--verbose> and overrides 
B<--quiet>.

=item B<--quiet>, B<-q>

If set, the script will report only fatal errors.

=item B<--verbose>, B<-v>

If set, the script will be more verbose about what it is doing.
Overrides B<--quiet>.

=item B<--help>, B<-h>

Output a the SYNOPSIS and OPTIONS section of the documentation and exit.

=item B<--version>

Output the version number and a copyright notice and exit.

=back
