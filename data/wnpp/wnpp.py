# Copyright (C) 2001, 2002, 2004  Martin Michlmayr <tbm@cyrius.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import string, sys, time
import re
import ldap

config = "/srv/qa.debian.org/data/wnpp/wnpp.conf"

exists = [ 'O', 'RFA', 'ITA', 'RFH' ]
wanted = [ 'ITP', 'RFP' ]
valid = exists + wanted

severities = {'O': ('normal', 'important'), 'RFA': ('normal',),
    'ITA': ('normal', 'important'), 'RFH': ('normal',), 'ITP': ('wishlist',),
    'RFP': ('wishlist',)}

proper_subject = re.compile("(\w+):\s+(\S+) -- (.*)")
weak_subject = re.compile("(\w+):\s+(\S+)")

def query(attrs=('debbugsID', 'debbugsTitle', 'debbugsState', 'debbugsSeverity')):
    try:
        wnpp = ldap.open('bts2ldap.debian.net', 10101)
    except ldap.LDAPError, error:
        print "Cannot connect to LDAP server: %s" % error[1]
        sys.exit(1)
    base = 'dc=current,dc=bugs,dc=debian,dc=org'
    try:
        wnpp.bind(base, '', ldap.AUTH_SIMPLE)
    except ldap.SERVER_DOWN:
        print "Cannot connect to LDAP server"
        sys.exit(1)
    result = wnpp.search_s(base, ldap.SCOPE_BASE, '(debbugsPackage=wnpp)', attrs)
    wnpp.unbind()

    return result

def date2days(date):
    return (int(time.time())-int(date))/86400

# vim: ts=4:expandtab:shiftwidth=4:
