#!/usr/bin/python3

# Check for common errors done in WNPP bug reports
# Copyright (C) 2001, 2002, 2003, 2004, 2005  Martin Michlmayr <tbm@cyrius.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import os, os.path, string, sys, tempfile
import apt_pkg, utils, wnpp

def sort_by_ext(a, b):
    return cmp(os.path.splitext(a)[1], os.path.splitext(b)[1])

Cnf = {}
Cnf["Dir::Templates"] = "/srv/qa.debian.org/data/wnpp/templates/"

packages = {}
binary_sources_mapping = {}
try:
    sources = os.popen("/srv/qa.debian.org/data/ftp/get-packages -s '{unstable,experimental}' -a source", "r")
except (IOError, OSError):
    print("Sources file not found")
    sys.exit(1)
Parse = apt_pkg.TagFile(sources)
while Parse.Step():
    source = Parse.Section.get("Package")
    if not source:
        continue
    packages[source] = 1
    for binary in map(string.strip, string.split(Parse.Section.get("Binary"), ",")):
        binary_sources_mapping[binary] = source
sources.close()

print("Querying WNPP via LDAP...")
result = wnpp.query(["debbugsID", "debbugsTitle", "debbugsState", "debbugsSeverity", "debbugsTag"])
print("Processing WNPP bugs...")
files = []
closes = []
for _, dict in result:
    if not dict:
        continue

    if dict["debbugsState"][0] == "done":
        continue

    bugid = int(dict["debbugsID"][0])
    subject = dict["debbugsTitle"][0]
    severity = dict["debbugsSeverity"][0]
    tags = dict.get("debbugsTag", [""])

    Subst = {}
    Subst["__BUG_NUMBER__"] = str(bugid)

    result = wnpp.proper_subject.search(subject)

    message = ""
    control = []
    close = []

    if result:
        tag = result.group(1)
        package = string.lower(result.group(2))
        description = result.group(3)
    else:
# FIXME: This check is disabled until wnpp.proper_subject is smarter
#        print("retitle %d " % bugid)
#        print("# %s does not contain a valid WNPP subject" % subject)
#        print("")
        continue

    if tag not in wnpp.valid:
        message += "# %s is not a valid tag\n" % tag
        message += "retitle %d XXX: %s -- %s\n" % (bugid, package, description)
        control.append("invalid_tag")
    else:

        if severity not in wnpp.severities[tag]:
            message += "# %s bugs have severity %s, not %s\n" % (tag,
                string.join(wnpp.severities[tag], " or "), severity)
            message += "severity %d %s\n" % (bugid, wnpp.severities[tag][0])
            control.append("severity")

        if tag in wnpp.exists and not packages.has_key(package):
            if package in binary_sources_mapping.keys():
                source = binary_sources_mapping[package]
                if tag != "RFH":
                    message += "# Binary package %s used instead of source package %s\n" % (package, source)
                    message += "retitle %s %s: %s -- %s\n" % (bugid, tag, source, description)
                    control.append("binary_source")
                package = source
            else:
                message += "# No package %s in Debian\n" % package
                message += "retitle %s %s: XXX -- %s\n" % (bugid, tag, description)
                control.append("no_package")

        if tag in wnpp.wanted and packages.has_key(package):
            if tag == "RFP":
                close.append("exists_now")
            elif "fixed-in-experimental" in tags:
                close.append("fixed_experimental")
            elif "fixed" not in tags:
                close.append("exists_already")

        # Search for bugs with the "fixed" tag which are mostly caused by
        # sponsored uploads.
        if "fixed" in tags:
            close.append("fixed_closed")

    if close and control:
        print("Bug #%d has both a close and a control command; please check manually." % bugid)
        continue
    if close:
        closes.append([bugid, package, close[0]])
    if control:
        # We create the control file with a specific extension because then
        # we can sort by extension and process control requests of the same
        # type at the same time.
        if len(control) > 1:
            ext = "mixed"
        else:
            ext = control[0]
        Subst["__CONTROL__"] = string.rstrip(message)
        template = utils.TemplateSubst(Subst, Cnf["Dir::Templates"]+"/wnpp-lint.control")
        filename = tempfile.mktemp("." + ext)
        try:
            output = open(filename, "w")
        except (IOError, OSError):
            print("E: Cannot write output file '%s'." % filename)
        output.write(template)
        output.close()
        files.append(filename)

package_info = {}
try:
    sources = os.popen("/srv/qa.debian.org/data/ftp/get-packages -s unstable -a source", "r")
except (IOError, OSError):
    print("Sources file not found")
    sys.exit(1)
Parse = apt_pkg.TagFile(sources)
while Parse.Step():
    package = Parse.Section.get("Package")
    if package in [x[1] for x in closes]:
        package_info[package] = ""
        for tag in Parse.Section.keys():
            package_info[package] += tag + ": " + Parse.Section.get(tag) + "\n"
sources.close()
for bugid, package, close in closes:
        Subst["__BUG_NUMBER__"] = str(bugid)
        Subst["__PACKAGE_INFO__"] = ""
        if package_info.has_key(package):
            Subst["__PACKAGE_INFO__"] = string.rstrip(package_info[package])
        else:
            print("No info for %s (#%d), maybe it is in experimental only." % (package, bugid))
            continue
        template = utils.TemplateSubst(Subst, Cnf["Dir::Templates"]+"/wnpp-lint.%s" % close)
        filename = tempfile.mktemp("." + close)
        try:
            output = open(filename, "w")
        except (IOError, OSError):
            print("E: Cannot write output file '%s'." % filename)
        output.write(template)
        output.close()
        files.append(filename)

files.sort(sort_by_ext)
for filename in files:
    result = os.system("/usr/bin/mutt -H %s" % filename)
    if result:
        print("Invocation of mutt failed.")
    os.unlink(filename)

# vim: ts=4:expandtab:shiftwidth=4:
