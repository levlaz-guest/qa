#!/usr/bin/perl -w
# vim:sw=4:

# Copyright 2005, 2006 Christoph Berg <myon@debian.org>
# Copied from the Debian PTS' control.pl

# Copyright 2002 Raphaël Hertzog <hertzog@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use MIME::Parser;
use MIME::Entity;
use Mail::Address;
use DB_File;

use strict;

# Mailbot for ddpo@qa.debian.org

=head1 INSTALLATION INSTRUCTIONS

This script needs libmime-perl and libmailtools-perl.

=cut

# Configuration variables
my $ddpo_dir = "/srv/qa.debian.org/data/ddpo/results";
my $db_filename = "/srv/qa.debian.org/ddpo/ddpo_subscribe.db";
my $sendmail = '/usr/sbin/sendmail -f ddpo@qa.debian.org';
my $sendmailnobody = '/usr/sbin/sendmail -f nobody@qa.debian.org';
#my $sources = "/srv/packages.qa.debian.org/www/incoming/sources";

# Global variables
my %db_content = ();
my $db = undef;
#my %bin2src = ();
#my %src = ();
my $open_count = 0;

# DB code
sub open_db_write {
    #$DB_BTREE->{'flags'} = R_DUP;
    if ($open_count <= 0) {
	$db = tie %db_content, "DB_File", $db_filename, O_RDWR|O_CREAT,
		      0660, $DB_BTREE
	    or die "Can't open database $db_filename : $!";
    }
    $open_count++;
}
sub open_db_read {
    #$DB_BTREE->{'flags'} = R_DUP;
    if ($open_count <= 0) {
	if (-f $db_filename) {
	    $db = tie %db_content, "DB_File", $db_filename, O_RDONLY,
			  0660, $DB_BTREE
		or die "Can't open database $db_filename : $!";
	} else {
	    open_db_write();
	}
    }
    $open_count++;
}
sub close_db {
    $open_count--;
    if ($open_count <= 0) {
	undef $db;
	untie %db_content;
    }
}

# data structures
sub explode {
    my ($address) = @_;
    my $tree = {};
    return $tree unless $db_content{$address};
    foreach my $section (split /;/, $db_content{$address}) {
	$section =~ /(\S+):(.*)/;
	foreach my $package (split / /, $2) {
	    $tree->{$1}->{$package} = 1;
	}
    }
    return $tree;
}

sub implode {
    my ($address, $tree) = @_;
    my @ret;
    foreach my $section (sort keys %$tree) {
	next unless keys %{$tree->{$section}};
	push @ret, ("$section:". join(' ', sort keys %{$tree->{$section}}));
    }
    return join ';', @ret;
}

# functions
sub subscribe {
    my ($address, $package, $section) = @_;
    open_db_write();
    my $tree = explode($address);
    foreach my $section (keys %$tree) {
	delete $tree->{$section}->{$package}; # delete from old section
    }
    $tree->{$section}->{$package} = 1;
    $db_content{$address} = implode($address, $tree);
    close_db();
}

sub unsubscribe {
    my ($address, $package) = @_;
    open_db_write();
    my $tree = explode($address);
    foreach my $section (keys %$tree) {
	delete $tree->{$section}->{$package}; # delete from old section
    }
    $db_content{$address} = implode($address, $tree);
    delete $db_content{$address} if( $db_content{$address} eq "" );
    close_db();
}

sub which {
    my ($address) = @_;
    my @msg = ("$address is subscribed to the following packages:\n");
    open_db_read();
    my $tree = explode(lc($address));
    foreach my $section (sort keys %$tree) {
	push @msg, " $section: " . join(' ', sort keys %{$tree->{$section}}) . "\n";
    }
    close_db();
    return @msg;
}

#sub load_sources {
#    return if (scalar(keys %bin2src));
#    open(SOURCES, "< $sources") || warn "Can't open $sources: $!\n";
#    while(defined($_=<SOURCES>)) {
#    	my ($bin, $comp, $src) = (split(/\s+/));
#	$bin2src{lc($bin)} = lc($src);
#	$src{lc($src)} = 1;
#    }
#    close(SOURCES);
#}

sub map_package {
    my ($pkg) = @_;
    my ($package, @msg);
    #load_sources();
    #if (exists $src{$pkg}) {
	$package = $pkg;
    #} elsif (exists $bin2src{$pkg}) {
    #    $package = $bin2src{$pkg};
    #    push @msg, "$pkg is not a source package. However $package is \n";
    #    push @msg, "the source package for the $pkg binary package.\n";
    #    push @msg, "\n";
    #} else {
    #	$package = $pkg;
    #    push @msg, "$pkg is neither a source package nor a binary package. \n";
    #    push @msg, "It may be a 'virtual package' or a mistake...\n";
    #    push @msg, "\n";
    #}
    return ($package, @msg);
}

# main code

# Parse the mail
my $parser = MIME::Parser->new();
$parser->output_to_core(1);
my $mail = $parser->parse(\*STDIN) or die "Parse failed !\n";

# Stop if bad X-Loop
my $xloop = $mail->head()->get('X-Loop');
if (defined($xloop) && ($xloop =~ /ddpo\@qa.debian.org/)) {
    exit 0;
}

# Extract the subject and the sender email
my $subject = $mail->head()->get("Subject") || "Your mail";
my $mid = $mail->head()->get("Message-ID") || "";
my $ref = $mail->head()->get("References") || "";
my ($email_obj) = Mail::Address->parse($mail->head()->get("From"));
my $email = $email_obj->address();
my $address = $email;

# Find the text/plain part containing commands...
if ($mail->is_multipart()) {
    $mail = $mail->parts(0); # Assume the first part is the interesting one
}

# Lines of the mail we got
my @lines = ("Subject: " . $subject, $mail->bodyhandle()->as_lines);

# Lines of the answer that we'll send
my @ans = ("Processing commands for ddpo\@qa.debian.org:\n", "\n");
my $nb_err = 0;
my ($done, $which);

foreach my $line (@lines) {
    push @ans, "> $line";
    
    # Hack for subject ...
    my $is_subject = 0;
    if ($line =~ /^Subject: (?:Re\s*:\s*)?(.*)$/i) {
	push @ans, "\n";
	$line = $1;
	$is_subject = 1;
    }
    
    # Try to detect commands
    if ($line =~ /^\s*(#|$)/) {
	next;
	
    } elsif ($line =~ /^\s*user\s+(\S+)/i) {
	$address = $1;
	push @ans, "Set user to $address.\n";
	$done++;

    # Allow spaces in section names: (needs postgres)
    #} elsif ($line =~ /^\s*subscribe\s+ (\S+) (?:\s+ (?: ([^"\s]+) | "([^"]+)" ) )?/ix) {
    } elsif ($line =~ /^\s*subscribe\s+(\S+)(?:\s+(\S+))?/i) {
	#my ($package, $section) = ($1, $2 || $3 || "ddpo");
	my ($package, $section) = ($1, $2 || "ddpo");
	my @explanation;
	($package, @explanation) = map_package($package);
	push @ans, @explanation;
	subscribe(lc($address), $package, $section);
	push @ans, "$package added to section $section.\n";
	$done++;
	$which = 1;
	
    } elsif ($line =~ /^\s*unsubscribe\s+(\S+)/i) {
	my ($package) = ($1);
	my @explanation;
	($package, @explanation) = map_package($package);
	push @ans, @explanation;
	unsubscribe($address, $package);
	unsubscribe(lc($address), $package);
	push @ans, "$package removed.\n";
	$done++;
	$which = 1;
	
    } elsif ($line =~ /^\s*which\b/i) {
	push @ans, which($address);
	$done++;
	$which = 0;
	
    #} elsif ($line =~ /^\s*unsubscribeall(?:\s+(\S+))?/i) {
	
    } elsif ($line =~ /^\s*help/i) {
	push @ans, <DATA>;
	$done++;
	$which = 0;

    } elsif ($line =~ /^(--|\s*quit|\s*thanks?|\s*txs|\s*end)/i) {
	push @ans, "Stopping processing here.\n";
	last;
	
    } else {
	# accept a few lines of garbage and then stop
	if (++$nb_err > 5) {
	    if ($done) {
	        push @ans, "Five lines without new commands: stopping.\n";
		push @ans, "$done command(s) successfully treated. Good bye !\n";
	    } else {
		#push @ans, "Too much garbage. Stopping.\n";
		# This really happens only for spam ... so do not reply
		exit 0;
	    }
	    last;
	}
	push @ans, "Unknown command.\n" unless $is_subject;
    }
}

if (!$done) {
	# No commands treated
	# Use "nobody" as sender to drop useless bounces
	#push @ans, "No command found in the message. Stopping.\n";
	#$sendmail = $sendmailnobody;
	exit 0;
}

if ($which) {
    push @ans, "\n", which($address);
}

# Prepare the subject for the answer
if ($subject !~ /^\s*Re: /) {
    $subject = "Re: $subject";
}

my $answer = MIME::Entity->build(From => 'DDPO Control Bot <ddpo@qa.debian.org>',
				 To => $email,
				 Subject => $subject,
				 Encoding => '8bit',
				 'X-Debian' => 'DDPO',
				 'X-Loop' => 'ddpo@qa.debian.org',
				 'References' => "$ref $mid",
				 'In-Reply-To:' => $mid,
				 Data => \@ans);

open(MAIL, "| $sendmail -oi -t") || die "Can't fork sendmail: $!\n";
$answer->print(\*MAIL);
close MAIL or die "Problem happened with sendmail: $!\n";

# Allow spaces in section names: (needs postgres)
# When quoted, <section> can contain whitespace.
#           subscribe sdate "Most important packages in Debian"

__DATA__

Debian Developer's Package Overview (DDPO)
------------------------------------------

The DDPO control bot accepts the following commands:

user <address>
  Process commands for <address>. Defaults to the address used in the From:
  header.
  Example: user cb@df7cb.de

subscribe <srcpackage> [<section>]
  Adds <srcpackage> to the listing. If <section> is omitted, defaults to
  "ddpo". This command can also be used to move <srcpackage> to a different
  section.
  Examples: subscribe mutt
            subscribe patch toolchain

  The special section "uploader" allows subscription to uploads made by a
  developer. This is useful when a DD is not using their @debian.org address
  for their packages. It is also possible to use a keyid here (8-char, prefixed
  by 0x; useful if the account was created recently and the projectb does not
  know about it yet). (Appending &uploader= to the URL gives the same result.)
  Example: subscribe myon@debian.org uploader

unsubscribe <srcpackage>
  Removes the <srcpackage> from the listing.
  Example: unsubscribe mutt

which
  Print the list of subscribed packages (automatically shown after
  subscribe/unsubscribe commands).

help
  Print this text.

quit
thanks
  Stops processing commands.

If you have questions, ask on <debian-qa@lists.debian.org>.
